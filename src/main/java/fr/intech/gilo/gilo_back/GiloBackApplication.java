package fr.intech.gilo.gilo_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiloBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiloBackApplication.class, args);
	}

}
